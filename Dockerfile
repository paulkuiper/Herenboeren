# Use an official Python runtime as a parent image
FROM python:latest

MAINTAINER pkuiper@gmail.com

# Set the working directory to /app
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install libs
RUN pip3 install --trusted-host pypi.python.org pipenv
RUN pipenv install --dev --system --deploy

# Run tests
CMD ["pytest", "--cov=./"]
#CMD ["python", "setup.py", "test"]