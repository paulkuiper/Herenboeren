import backend

def test_import_plugins():
    assert backend.generate_table_name('User') == 'users'
    assert backend.generate_table_name('Company') == 'companies'
    assert backend.generate_table_name('bus') == 'buses'