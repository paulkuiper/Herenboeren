import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="herenboeren",
    version="0.0.1",
    author="Paul Kuiper",
    author_email="pkuiper@gmail.com",
    description="Web app to manage herenboeren farms",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/paulkuiper/Herenboeren",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    setup_requires=["pytest-runner"],
    tests_require=["pytest","pytest-cov"],
)